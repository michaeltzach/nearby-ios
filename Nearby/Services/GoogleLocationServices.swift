//
//  GoogleLocationServices.swift
//  Nearby
//
//  Created by Michael tzach on 04/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class GoogleLocationServices: NSObject {
    static let nearbySearchKey = "AIzaSyDA4kmV0ba3HLnFAFYwRWfG9iej8YxxY7E"
    static let nearbySearchBaseURL = URL.init(string:"https://maps.googleapis.com/maps/api/place/nearbysearch/json")!
    
    static let autocompleteAPIKey = "AIzaSyANiJGcQ0x_tOUNnH6pHKkw5TZ9vC_xmd4"
    static let autocompleteBaseURL = URL.init(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json")!
    
    static let placeDetailsKey = "AIzaSyDNfbBieAzHqzliMGNbQVst6rn9EY5gU-w"
    static let placeDetailsBaseURL = URL.init(string: "https://maps.googleapis.com/maps/api/place/details/json")!
    
    private static let jsonDecoder = JSONDecoder()
    
    static func searchNearbyPlaces(aroundLocation location: CLLocationCoordinate2D, completionBlock: @escaping (NearbyPlacesSearchResults?, Error?)->Void) {
        let parameters: [String: String] = [
            "key": nearbySearchKey,
            "location": "\(location.latitude),\(location.longitude)",
            "radius": "500"
        ]
        
        Alamofire.request(nearbySearchBaseURL, method: .get, parameters: parameters).responseData { response in
            guard let jsonData = response.data else {
                completionBlock(nil, response.error)
                return
            }
            let results = try! jsonDecoder.decode(NearbyPlacesSearchResults.self, from: jsonData)

            completionBlock(results, nil)
        }
    }
    
    static func searchAutocomplete(searchTerm: String, aroundLocation location: CLLocationCoordinate2D, completionBlock: @escaping (_ result: AutoCompleteResults?, _ error: Error?) -> Void) {
        let parameters: [String: Any] = [
            "key": autocompleteAPIKey,
            "location": "\(location.latitude),\(location.longitude)",
            "radius": "2500",
            "types": "establishment",
            "strictbounds": NSNull(),
            "input": searchTerm.replacingOccurrences(of: " ", with: "+")
        ]
        
        Alamofire.request(autocompleteBaseURL, method: .get, parameters: parameters).responseData { (response) in
            guard let jsonData = response.data else {
                completionBlock(nil, response.error)
                return
            }
            let results = try! jsonDecoder.decode(AutoCompleteResults.self, from: jsonData)
            
            completionBlock(results, nil)
        }
    }
    
    static func placeDetails(placeReference ref: String, completionBlock: @escaping (_ result: PlaceDetailsResponse?, _ error: Error?) -> Void) {
        let parameters: [String: Any] = [
            "key": placeDetailsKey,
            "reference": ref
        ]
        
        Alamofire.request(placeDetailsBaseURL, method: .get, parameters: parameters).responseData { (response) in
            guard let jsonData = response.data else {
                completionBlock(nil, response.error)
                return
            }
            let results = try! jsonDecoder.decode(PlaceDetailsResponse.self, from: jsonData)
            
            completionBlock(results, nil)
        }
    }
}
