//
//  PlaceDetailsResponse.swift
//  Nearby
//
//  Created by Michael tzach on 04/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import Foundation

struct PlaceDetailsResult: Codable {
    let geometry: GooglePlaceServicesGeometry
    let icon: URL
    let name: String
}

struct PlaceDetailsResponse: Codable {
    let result: PlaceDetailsResult
}
