//
//  AutoCompleteResults.swift
//  Nearby
//
//  Created by Michael tzach on 04/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import Foundation

struct AutoCompletePrediction: Codable {
    let description: String
    let reference: String
}

struct AutoCompleteResults: Codable {
    let predictions: [AutoCompletePrediction]
}
