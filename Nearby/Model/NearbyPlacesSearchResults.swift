//
//  NearbyPlacesSearchResults.swift
//  Nearby
//
//  Created by Michael tzach on 04/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import Foundation

struct NearbyPlacesSearchLocation: Codable {
    let lat: Double
    let lng: Double
}

struct GooglePlaceServicesGeometry: Codable {
    let location: NearbyPlacesSearchLocation
}

struct NearbyPlacesSearchResult: Codable {
    let name: String
    let geometry: GooglePlaceServicesGeometry
}

struct NearbyPlacesSearchResults: Codable {
    let results: [NearbyPlacesSearchResult]
}
