//
//  UIVIewController+Utils.swift
//  Nearby
//
//  Created by Michael tzach on 03/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit

extension UIViewController {
    func navigationControllerSurroundingViewController() -> UINavigationController {
        let navigationController = UINavigationController.init(rootViewController: self)
        return navigationController
    }
}
