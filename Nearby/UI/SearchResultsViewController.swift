//
//  SearchResultsViewController.swift
//  Nearby
//
//  Created by Michael tzach on 03/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import SnapKit
import CoreLocation

protocol SearchResultsViewControllerDelegate: AnyObject {
    func searchResultsViewController(_ controller: SearchResultsViewController, changedOpenStateTo openState: Bool)
    func searchResultsViewController(_ controller: SearchResultsViewController, didSelectNearbyLocation location: NearbyPlacesSearchResult)
}

class SearchResultsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    private let titleHeight: CGFloat = 18
    weak var delegate: SearchResultsViewControllerDelegate? = nil
    
    private var isOpen = false {
        didSet {
            UIView.animate(withDuration: 0.3) {
                if (self.isOpen) {
                    self.openCloseViewButton.transform = CGAffineTransform.identity.rotated(by: CGFloat(Double.pi))
                } else {
                    self.openCloseViewButton.transform = CGAffineTransform.identity
                }
            }
        }
    }
    
    var results: NearbyPlacesSearchResults? = nil
    var didSearch = false
    
    lazy var titleButton: UIButton = {
       let button = UIButton.init(type: .custom)
        button.addTarget(self, action: #selector(openCloseButtonPressed), for: .touchUpInside)
        button.setTitle("Nearby places", for: .normal)
        button.setTitleColor(.black, for: .normal)
        return button
    }()
    
    lazy var openCloseViewButton: UIButton = {
       let button = UIButton.init(type: .custom)
        
        button.setImage(UIImage.init(named: "ic_keyboard_arrow_up"), for: .normal)
        button.addTarget(self, action: #selector(openCloseButtonPressed), for: .touchUpInside)
        
        return button
    }()
    
    private let cellIdentifier = "SearchResultsViewControllerCellIdentifier"
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        return indicator
    }()
    
    lazy var searchResultsTableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        return tableView
    }()
    
    func heightNeededWhenClosed() -> CGFloat {
        return titleHeight + 10 * 2.0
    }
    
    func searchAroundLocation(_ location: CLLocationCoordinate2D) {
        //TODO: now there is only one search. we need to think how we throttle this thing
        if (didSearch) { return }
        didSearch = true
        
        results = nil
        searchResultsTableView.reloadData()
        
        loadingIndicator.startAnimating()
        
        GoogleLocationServices.searchNearbyPlaces(aroundLocation: location) { (results, error) in
            self.loadingIndicator.stopAnimating()
            
            if error != nil {
                //TODO: show error
                return
            }
            
            self.results = results
            self.searchResultsTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        view.addSubview(searchResultsTableView)
        view.addSubview(titleButton)
        view.addSubview(loadingIndicator)
        view.addSubview(openCloseViewButton)
        
        titleButton.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(10)
            make.leading.equalTo(view).offset(10)
            make.height.equalTo(titleHeight)
            make.trailing.equalTo(openCloseViewButton.snp.leading)
        }
        openCloseViewButton.snp.makeConstraints { (make) in
            make.height.equalTo(titleButton)
            make.top.equalTo(titleButton)
            make.trailing.equalTo(view).offset(-10)
        }
        searchResultsTableView.snp.makeConstraints { (make) in
            make.top.equalTo(titleButton.snp.bottom).offset(10)
            make.leading.equalTo(view)
            make.trailing.equalTo(view)
            make.bottom.equalTo(view)
        }
        loadingIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(searchResultsTableView)
        }
    }

    //MARK: UITableViewDataSource, UITableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let results = self.results else { return 0 }
        
        return results.results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.selectionStyle = .none
        
        cell.textLabel?.text = results?.results[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let searchResult = results?.results[indexPath.row] else { return }
        delegate?.searchResultsViewController(self, didSelectNearbyLocation: searchResult)
    }
    
    @objc func openCloseButtonPressed() {
        isOpen = !isOpen
        delegate?.searchResultsViewController(self, changedOpenStateTo: isOpen)
    }

}
