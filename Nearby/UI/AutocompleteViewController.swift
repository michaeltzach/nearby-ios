//
//  AutocompleteViewController.swift
//  Nearby
//
//  Created by Michael tzach on 04/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import SnapKit
import CoreLocation

protocol AutocompleteViewControllerDelegate: AnyObject {
    func viewController(_ viewController: AutocompleteViewController, didSelectAutoCompletePrediction prediction: AutoCompletePrediction)
}

class AutocompleteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    weak var delegate: AutocompleteViewControllerDelegate? = nil
    
    private var results: AutoCompleteResults? = nil

    private let throttleUniqueId = UUID().uuidString

    private let autocompleteViewControllerCellIdentifier = "autocompleteViewControllerCellIdentifier"
    
    lazy var tableView: UITableView = {
       let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: autocompleteViewControllerCellIdentifier)
        tableView.separatorStyle = .none
        return tableView
    }()
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        return indicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(tableView)
        view.addSubview(loadingIndicator)

        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        loadingIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(view)
        }
        // Do any additional setup after loading the view.
    }

    static func tableViewHeight() -> CGFloat {
        return 130
    }
    
    func search(_ phrase: String, aroundLocation location: CLLocationCoordinate2D) {
        results = nil
        tableView.reloadData()
        
        loadingIndicator.startAnimating()
        
        AutocompleteDataHandler.shared.throttleAndSearchAutoComplete(phrase: phrase, aroundLocation: location, throttlingUniqueId: throttleUniqueId) { (response, error) in
            self.loadingIndicator.stopAnimating()
            
            if error != nil {
                //TODO: do something
                return
            }
            
            self.results = response
            self.tableView.reloadData()
        }
    }
    
    //MARK: UITableViewDelegate, UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let autoCompleteResults = results {
            if autoCompleteResults.predictions.count > 3 {
                return 3
            }
            return autoCompleteResults.predictions.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: autocompleteViewControllerCellIdentifier, for: indexPath)
        cell.selectionStyle = .none
        
        if let model = results {
            cell.textLabel?.text = model.predictions[indexPath.row].description
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = results {
            let prediction = model.predictions[indexPath.row]
            delegate?.viewController(self, didSelectAutoCompletePrediction: prediction)
        }
    }
}
