//
//  UISearchTextField.swift
//  Nearby
//
//  Created by Michael tzach on 04/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit

class SeachTextField: UITextField {
    lazy var cancelButton: UIButton = {
        let cancelButton = UIButton.init(type: .custom)
        cancelButton.setImage(UIImage.init(named: "ic_cancel"), for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelButtonPressedInSearchBar), for: .touchUpInside)
        return cancelButton
    }()
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        let height = bounds.size.height * 2 / 3;
        let width = height
        let top = (bounds.size.height - height) / 2
        return CGRect.init(x: 7, y: top, width: width, height: height)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        let height = bounds.size.height * 2 / 3;
        let width = height
        let top = (bounds.size.height - height) / 2
        let left = bounds.size.width - width - 7
        return CGRect.init(x: left, y: top, width: width, height: height)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.25
        layer.cornerRadius = 3
        backgroundColor = .white
        minimumFontSize = 14
        
        let leftView = UIImageView.init(image: UIImage.init(named: "ic_search"))
        leftViewMode = .always
        self.leftView = leftView
        
        rightViewMode = .whileEditing
        self.rightView = cancelButton
    }
    
    @objc func cancelButtonPressedInSearchBar() {
        var shouldResignFirstResponder = true
        if let text = text, text.count > 0 {
            shouldResignFirstResponder = false
        }
        
        text = nil
        
        if shouldResignFirstResponder {
            resignFirstResponder()
        }
    }
}
