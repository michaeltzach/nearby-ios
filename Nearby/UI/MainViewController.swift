
//
//  MainViewController.swift
//  Nearby
//
//  Created by Michael tzach on 03/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import STLocationRequest
import CoreLocation
import MapKit
import SnapKit

enum MainViewControllerErrorType {
    case locationDenied
    case locationDisabled
}

class MainViewController: UIViewController, STLocationRequestControllerDelegate, MKMapViewDelegate, UITextFieldDelegate, AutocompleteViewControllerDelegate, SearchResultsViewControllerDelegate {
    
    private var userLocation: MKUserLocation? = nil
    private var searchControllerTopConstraint: Constraint? = nil
    private var currentAutocompleteSelection: String? = nil
    private let mapViewAnnotationIdentifier = "mapViewAnnotationIdentifier"
    
    lazy var autoCompleteViewController: AutocompleteViewController = {
        let viewController = AutocompleteViewController()
        viewController.view.isHidden = true
        viewController.delegate = self
        return viewController
    }()
    var autoCompleteViewControllerHeightConstraint: Constraint? = nil
    
    lazy var searchTextField: SeachTextField = {
        let textField = SeachTextField.init()
        textField.delegate = self
        textField.addTarget(self, action: #selector(textFieldDidChange), for: UIControlEvents.editingChanged)
        textField.placeholder = "Search other places"
        return textField
    }()
    
    lazy var searchResultsController: SearchResultsViewController = {
        let viewController = SearchResultsViewController()
        viewController.delegate = self
        return viewController
    }()
    
    lazy var mapView: MKMapView = {
        let mapView = MKMapView.init(frame: self.view.bounds)
        mapView.isScrollEnabled = true
        mapView.isZoomEnabled = true
        mapView.showsScale = false
        mapView.showsCompass = false
        mapView.showsTraffic = false
        mapView.showsUserLocation = true
        mapView.mapType = .standard
        mapView.delegate = self
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: mapViewAnnotationIdentifier)
        return mapView
    }()
    
    //MARK: MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        self.userLocation = userLocation
        
        searchResultsController.searchAroundLocation(userLocation.coordinate)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let markerView = mapView.dequeueReusableAnnotationView(withIdentifier: mapViewAnnotationIdentifier, for: annotation)
        
        return markerView;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        view.addSubview(mapView)
        
        addChildViewController(autoCompleteViewController)
        view.addSubview(autoCompleteViewController.view)
        
        view.addSubview(searchTextField)
        
        addChildViewController(searchResultsController)
        view.addSubview(searchResultsController.view)
        
        
        mapView.snp.makeConstraints { (make) in
            make.edges.equalTo(view);
        }
        searchTextField.snp.makeConstraints { (make) in
            make.top.equalTo(30)
            make.leading.equalTo(view).inset(15)
            make.trailing.equalTo(view).inset(15)
            make.height.equalTo(40)
        }
        autoCompleteViewController.view.snp.makeConstraints { (make) in
            make.leading.equalTo(searchTextField.snp.leading)
            make.width.equalTo(searchTextField.snp.width)
            make.top.equalTo(searchTextField.snp.bottom)
            make.height.equalTo(AutocompleteViewController.tableViewHeight())
        }
        searchResultsController.view.snp.makeConstraints { (make) in
            make.bottom.equalTo(view)
            make.leading.equalTo(view).inset(15)
            make.trailing.equalTo(view).inset(15)
            searchControllerTopConstraint = make.top.equalTo(view.snp.bottom).offset(searchResultsController.heightNeededWhenClosed() * -1).constraint
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        checkLocationServicesStatusAndSetPresentedViewController()
    }
    
    func checkLocationServicesStatusAndSetPresentedViewController() {
        if !CLLocationManager.locationServicesEnabled() {
            embbedErrorView(forErrorType: .locationDisabled)
            return
        }
        
        switch CLLocationManager.authorizationStatus() {
        case .denied:
            embbedErrorView(forErrorType: .locationDenied)
        case .restricted:
            embbedErrorView(forErrorType: .locationDisabled)
        case .notDetermined:
            presentLocationRequestController()
        case .authorizedAlways, .authorizedWhenInUse:
            break
        }
    }
    
    func embbedErrorView(forErrorType type: MainViewControllerErrorType) {
        print("We have an error of type \(type)")
    }
    
    func presentLocationRequestController(){
        let locationRequestController = STLocationRequestController.getInstance()
        locationRequestController.titleText = "We need your location to show you great places around you"
        locationRequestController.allowButtonTitle = "Alright"
        locationRequestController.notNowButtonTitle = "Not now"
        locationRequestController.authorizeType = .requestWhenInUseAuthorization
        locationRequestController.delegate = self
        locationRequestController.isPulseEffectEnabled = true
        locationRequestController.isLocationSymbolHidden = true
        locationRequestController.addPlace(latitude: 32.0734342, longitude: 34.7761237) //My house
        locationRequestController.placesFilter = [.customPlaces]
        locationRequestController.present(onViewController: self)
    }
    
    //MARK: STLocationRequestControllerDelegate
    
    func locationRequestControllerDidChange(_ event: STLocationRequestControllerEvent) {
        checkLocationServicesStatusAndSetPresentedViewController()
    }
    
    //MARK: UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateHideAutoComplete()
    }
    
    func animateShowAutoComplete() {
        self.autoCompleteViewController.view.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.autoCompleteViewController.view.alpha = 1
            self.autoCompleteViewController.view.isOpaque = true
        }
    }
    
    func animateHideAutoComplete() {
        self.autoCompleteViewController.view.isOpaque = false
        UIView.animate(withDuration: 0.2, animations: {
            self.autoCompleteViewController.view.alpha = 0
        }) { (finished) in
            self.autoCompleteViewController.view.isHidden = true
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else { return }
        
        if (text == currentAutocompleteSelection) { return }
        currentAutocompleteSelection = nil
        
        //TODO: if there is no location, show the user an error message
        guard let userLocation = userLocation?.coordinate else { return }
        
        if text.count > 0 {
            animateShowAutoComplete()
        } else {
            animateHideAutoComplete()
        }
        
        autoCompleteViewController.search(text, aroundLocation: userLocation)
    }
    
    //MARK: AutocompleteViewControllerDelegate
    func viewController(_ viewController: AutocompleteViewController, didSelectAutoCompletePrediction prediction: AutoCompletePrediction) {
        GoogleLocationServices.placeDetails(placeReference: prediction.reference) { (response, error) in
            if error != nil {
                //TODO: handle this
                return
            }
            
            guard let result = response?.result else { return }
            
            self.currentAutocompleteSelection = result.name
            self.searchTextField.text = result.name
            
            self.searchTextField.resignFirstResponder()
            
            let centerCoordinate = CLLocationCoordinate2D.init(latitude: result.geometry.location.lat, longitude: result.geometry.location.lng)

            self.addAnnotation(coordinate: centerCoordinate, name: result.name)
        }
    }
    
    private func addAnnotation(coordinate: CLLocationCoordinate2D, name: String) {
        let annotation = MKPointAnnotation()

        annotation.coordinate = coordinate
        annotation.title = name
        self.mapView.addAnnotation(annotation)
        
        //Zoom and center
        self.mapView.setCenter(coordinate, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) {
            let zoomedRegion = MKCoordinateRegion.init(center: self.mapView.region.center, span: MKCoordinateSpan.init(latitudeDelta: 0.03, longitudeDelta: 0.03))
            self.mapView.setRegion(zoomedRegion, animated: true)
        }
    }
    
    //MARK: SearchResultsViewControllerDelegate
    
    func searchResultsViewController(_ controller: SearchResultsViewController, changedOpenStateTo openState: Bool) {
        UIView.animate(withDuration: 0.2) {
            if (openState) {
                self.searchControllerTopConstraint?.update(offset: -300)
            } else {
                self.searchControllerTopConstraint?.update(offset: self.searchResultsController.heightNeededWhenClosed() * -1)
            }
            self.view.layoutIfNeeded()
        }
    }
    
    func searchResultsViewController(_ controller: SearchResultsViewController, didSelectNearbyLocation location: NearbyPlacesSearchResult) {
        let centerCoordinate = CLLocationCoordinate2D.init(latitude: location.geometry.location.lat, longitude: location.geometry.location.lng)
        addAnnotation(coordinate: centerCoordinate, name: location.name)
    }
}
