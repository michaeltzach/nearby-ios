//
//  AutocompleteDataHandler.swift
//  Nearby
//
//  Created by Michael tzach on 04/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import CoreLocation

class AutocompleteDataHandler: NSObject {

    private var autoCompleteCache = [String: AutoCompleteResults]()
    private var throttles = [String: Timer]()
    
    static let shared = AutocompleteDataHandler()
    
    func throttleAndSearchAutoComplete(phrase :String,
                                       aroundLocation location: CLLocationCoordinate2D,
                                       throttlingUniqueId throttleId: String, completionBlock: @escaping (_ results: AutoCompleteResults?, _ error: Error?)->()) {
        //Get from cache
        if let cachedResults = autoCompleteCache[phrase] {
            completionBlock(cachedResults, nil)
            return
        }
        
        if let throttle = throttles[throttleId] {
            throttle.invalidate()
        }
        
        throttles[throttleId] = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (timer) in
            self.throttles.removeValue(forKey: throttleId)
            
            GoogleLocationServices.searchAutocomplete(searchTerm: phrase, aroundLocation: location, completionBlock: { (results, error) in
                if error != nil {
                    completionBlock(nil, error)
                    return
                }
                
                self.autoCompleteCache[phrase] = results
                
                completionBlock(results, error)
            })
        })
    }
    
}
