//
//  LocationHandler.swift
//  Nearby
//
//  Created by Michael tzach on 03/12/2017.
//  Copyright © 2017 Michael tzach. All rights reserved.
//

import UIKit
import CoreLocation

class LocationHandler: NSObject {

    static let shared = LocationHandler()

    
    func locationServicesAreEnabled() -> Bool{
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .denied {
                // Location Services are denied
            } else {
                if CLLocationManager.authorizationStatus() == .notDetermined{
                    // Present the STLocationRequestController
                    
                } else {
                    // The user has already allowed your app to use location services. Start updating location
                }
            }
        } else {
            // Location Services are disabled
        }
        return false
    }
}
